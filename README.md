
# How to Build

WORKDIR=<some_dir>

mkdir -p $WORKDIR

cd $WORKDIR

git clone https://gitlab.com/sgalabov1/prplos.git

git clone https://gitlab.com/sgalabov1/prplmesh-configs.git

cd prplos

git checkout mediatek-prplos-22.03

ln -s ../prplmesh-configs/files/ files

./scripts/gen_config xcpe-ax30-01 prpl

make -j$(nproc)

Artefacts will be in bin/targets/mediatek/filogic/ .

openwrt-mediatek-filogic-smartcom_xcpe-ax30-01-squashfs-factory.bin

Use the above file to update via U-Boot.

# Sample Configs for prplMesh

This repo contains sample prplmesh config scripts used for testing prplmesh on Banana Pi BPI-R3.

Currently the configs were tested with wired backhaul only with the agent's lan1 port connected to a lan port on the controller.

The scripts are contained within files/root/ directory, so it would be easy to include them in a prplOS build by just creating a symlink to them.

## Gateway Initial Config

Upon first boot execute /root/gateway.sh.

## Repeater Initial Config

Upon first boot execute /root/repeater.sh.

NB: Doesn't work currently as of prplOS 3.0.0 due to uci no longer being used for LAN address assignment. Need to see how to assign LAN IP address via TR-181.

NB: If you have more than one repeater be sure to edit repeater.sh to set a proper static IP address for the given repeater. Specifically edit the following line:
uci set network.lan.ipaddr='192.168.1.2/24'

## prplMesh Provisioning

Upon each reboot (for now, until prplMesh config becomes persistent) and after ensuring that prplMesh has properly started on both the gateway and the agents, do the following on the controller:
cd root; ./config.sh; ./hw_offload.sh
on agents:
cd root; ./hw_offload.sh
