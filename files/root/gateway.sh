#!/bin/sh

# Change prplmesh config to match new interface naming convention
#uci set prplmesh.radio0.hostap_iface='phy1-ap0'
#uci set prplmesh.radio0.hostap_iface_steer_vaps='phy1-ap1'
#uci set prplmesh.radio0.sta_iface='phy1-ap0'
#uci set prplmesh.radio1.hostap_iface='phy0-ap0'
#uci set prplmesh.radio1.hostap_iface_steer_vaps='phy0-ap1'
#uci set prplmesh.radio1.sta_iface='phy0-ap0'

# Disable bridger
/etc/inid.d/bridger disable
/etc/init.d/bridger stop

# Enable prplmesh and start it up in gateway mode
/etc/init.d/prplmesh enable
service prplmesh gateway_mode
