#!/bin/sh

# Change prplmesh config to match new interface naming convention
#uci set prplmesh.radio0.hostap_iface='phy1-ap0'
#uci set prplmesh.radio0.hostap_iface_steer_vaps='phy1-ap1'
#uci set prplmesh.radio0.sta_iface='phy1-ap0'
#uci set prplmesh.radio1.hostap_iface='phy0-ap0'
#uci set prplmesh.radio1.hostap_iface_steer_vaps='phy0-ap1'
#uci set prplmesh.radio1.sta_iface='phy0-ap0'

# Set static LAN address (if using more agents this needs to be edited)
ubus-cli -a Device.IP.Interface.lan.IPv4Address.lan.IPAddress="192.168.1.2"

# Stop DHCP servers
ubus-cli -a Device.DHCPv4.Server.Enable=0
ubus-cli -a Device.DHCPv6.Server.Enable=0

# Stop DNS Server
ubus-cli -a Device.DNS.Relay.Enable=0

# Restart network, so settings take effect
#service network restart

# Enable bridger
/etc/init.d/bridger enable
/etc/init.d/bridger start

# Enable prplmesh and start it up in repeater mode
/etc/init.d/prplmesh enable
service prplmesh repeater_mode
